USE [master]
GO
/****** Object:  Database [InventoryManagementDB]    Script Date: 1/30/2019 6:30:50 PM ******/
CREATE DATABASE [InventoryManagementDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'InventoryManagementDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\InventoryManagementDB.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'InventoryManagementDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\InventoryManagementDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [InventoryManagementDB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [InventoryManagementDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [InventoryManagementDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [InventoryManagementDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [InventoryManagementDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [InventoryManagementDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [InventoryManagementDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [InventoryManagementDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [InventoryManagementDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [InventoryManagementDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [InventoryManagementDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [InventoryManagementDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [InventoryManagementDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [InventoryManagementDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [InventoryManagementDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [InventoryManagementDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [InventoryManagementDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [InventoryManagementDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [InventoryManagementDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [InventoryManagementDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [InventoryManagementDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [InventoryManagementDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [InventoryManagementDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [InventoryManagementDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [InventoryManagementDB] SET RECOVERY FULL 
GO
ALTER DATABASE [InventoryManagementDB] SET  MULTI_USER 
GO
ALTER DATABASE [InventoryManagementDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [InventoryManagementDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [InventoryManagementDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [InventoryManagementDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [InventoryManagementDB] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'InventoryManagementDB', N'ON'
GO
USE [InventoryManagementDB]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 1/30/2019 6:30:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Categories](
	[CategoryId] [bigint] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Groups]    Script Date: 1/30/2019 6:30:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Groups](
	[GroupId] [bigint] IDENTITY(1,1) NOT NULL,
	[GroupNm] [varchar](50) NOT NULL,
	[PartyNm] [varchar](50) NOT NULL,
	[Address] [varchar](100) NOT NULL,
	[TelephoneNo] [varchar](20) NULL,
	[MobileNo] [varchar](20) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[WebAddress] [varchar](100) NULL,
	[ContactPersonNm] [nchar](10) NULL,
	[ContactMobileNo] [varchar](20) NULL,
	[Remark] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Items]    Script Date: 1/30/2019 6:30:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Items](
	[ItemId] [bigint] IDENTITY(1,1) NOT NULL,
	[ItemName] [varchar](50) NOT NULL,
	[BrandName] [varchar](50) NOT NULL,
	[Unit] [int] NOT NULL,
	[BuyRate] [int] NOT NULL,
	[SaleRate] [int] NOT NULL,
	[Qty] [int] NOT NULL,
	[Discount] [int] NULL,
	[PCQty] [int] NOT NULL,
	[CategoryId] [bigint] NOT NULL,
 CONSTRAINT [PK_Items] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MasterStocks]    Script Date: 1/30/2019 6:30:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MasterStocks](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Date] [date] NOT NULL,
	[MemoNo] [int] NOT NULL,
	[Year] [int] NOT NULL,
	[Type] [varchar](20) NOT NULL,
	[StoreId] [bigint] NOT NULL,
	[GroupId] [bigint] NOT NULL,
 CONSTRAINT [PK_MasterStocks] PRIMARY KEY CLUSTERED 
(
	[MemoNo] ASC,
	[Year] ASC,
	[Type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Stocks]    Script Date: 1/30/2019 6:30:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Stocks](
	[CategoryId] [bigint] NOT NULL,
	[ItemId] [bigint] NOT NULL,
	[Year] [int] NOT NULL,
	[MemoNo] [int] NOT NULL,
	[Type] [varchar](20) NOT NULL,
	[ItemSL] [int] NOT NULL,
	[Quantity] [int] NULL,
	[Rate] [int] NULL,
	[Amount] [int] NULL,
	[Discount] [int] NULL,
	[GrossAmount] [int] NULL,
 CONSTRAINT [PK_Stocks] PRIMARY KEY CLUSTERED 
(
	[Year] ASC,
	[MemoNo] ASC,
	[Type] ASC,
	[ItemSL] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Stores]    Script Date: 1/30/2019 6:30:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Stores](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[StoreName] [varchar](50) NOT NULL,
	[StoreId] [bigint] NOT NULL,
	[Remark] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[Purchases]    Script Date: 1/30/2019 6:30:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create VIEW [dbo].[Purchases] AS 
SELECT ItemSL, i.ItemName, Quantity, Amount, s.Discount, GrossAmount, Year, Type, MemoNo FROM Stocks AS s INNER JOIN Items AS i ON s.ItemId=i.ItemId
GO
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([CategoryId], [CategoryName]) VALUES (35, N'Electronics')
INSERT [dbo].[Categories] ([CategoryId], [CategoryName]) VALUES (66, N'Consumer')
INSERT [dbo].[Categories] ([CategoryId], [CategoryName]) VALUES (67, N'Food')
INSERT [dbo].[Categories] ([CategoryId], [CategoryName]) VALUES (68, N'Cosmetics')
INSERT [dbo].[Categories] ([CategoryId], [CategoryName]) VALUES (69, N'Cosmetices')
INSERT [dbo].[Categories] ([CategoryId], [CategoryName]) VALUES (70, N'Furniture')
INSERT [dbo].[Categories] ([CategoryId], [CategoryName]) VALUES (80, N'Medicine')
INSERT [dbo].[Categories] ([CategoryId], [CategoryName]) VALUES (81, N'home appliance')
INSERT [dbo].[Categories] ([CategoryId], [CategoryName]) VALUES (99, N'')
SET IDENTITY_INSERT [dbo].[Categories] OFF
SET IDENTITY_INSERT [dbo].[Groups] ON 

INSERT [dbo].[Groups] ([GroupId], [GroupNm], [PartyNm], [Address], [TelephoneNo], [MobileNo], [Email], [WebAddress], [ContactPersonNm], [ContactMobileNo], [Remark]) VALUES (1, N'Supplier', N'Helal', N'xvbzvbv', N'xcbzc', N'xbzzzzzzbvx', N'xcbx@gmail.com', N'dvsfdsd', N'sdfsdfs   ', N'sdfsdfs', N'sdfsdf')
INSERT [dbo].[Groups] ([GroupId], [GroupNm], [PartyNm], [Address], [TelephoneNo], [MobileNo], [Email], [WebAddress], [ContactPersonNm], [ContactMobileNo], [Remark]) VALUES (2, N'Party', N'Mobarak', N'cvbxcb', N'cvbvbcxb', N'bcvbcvbcvb', N'bvbcvb@gmail.com', N'bvcbcb', N'cvbcvbv   ', N'cvbcvv', N'bbbbcvbcvb')
INSERT [dbo].[Groups] ([GroupId], [GroupNm], [PartyNm], [Address], [TelephoneNo], [MobileNo], [Email], [WebAddress], [ContactPersonNm], [ContactMobileNo], [Remark]) VALUES (3, N'Party', N'Ashraf', N'fgfdgdf', N'fgdfsgd', N'dfgdsfgd', N'fdggfdgsdf@gmail.com', N'sfsdfsdfd', N'dsfsdf    ', N'dfsdf', N'sdfsdaf')
INSERT [dbo].[Groups] ([GroupId], [GroupNm], [PartyNm], [Address], [TelephoneNo], [MobileNo], [Email], [WebAddress], [ContactPersonNm], [ContactMobileNo], [Remark]) VALUES (4, N'Supplier', N'Sakib', N'fdgdfg', N'dfgfdgdf', N'fdgdfgdf', N'fdgdfgf@gmail.com', N'fdgdfgdf', N'fdgdfgfsdg', N'fdgdfgdf', N'dfgdfgdf')
INSERT [dbo].[Groups] ([GroupId], [GroupNm], [PartyNm], [Address], [TelephoneNo], [MobileNo], [Email], [WebAddress], [ContactPersonNm], [ContactMobileNo], [Remark]) VALUES (6, N'Supplier', N'Hanif', N'cdsdc', N'adasasdas', N'daaAS', N'dasddas@gmail.com', N'czxxz', N'zxczxcxz  ', N'ASDASDAS', N'zxczxcz')
INSERT [dbo].[Groups] ([GroupId], [GroupNm], [PartyNm], [Address], [TelephoneNo], [MobileNo], [Email], [WebAddress], [ContactPersonNm], [ContactMobileNo], [Remark]) VALUES (5, N'Supplier', N'Yusouf', N'fdgdfg', N'dfgfdgdf', N'fdgdfgdf', N'fdgdfgf@gmail.com', N'fdgdfgdf', N'fdgdfgfsdg', N'fdgdfgdf', N'dfgdfgdf')
INSERT [dbo].[Groups] ([GroupId], [GroupNm], [PartyNm], [Address], [TelephoneNo], [MobileNo], [Email], [WebAddress], [ContactPersonNm], [ContactMobileNo], [Remark]) VALUES (7, N'Supplier', N'Rahim', N'gol paharh', N'3892739', N'01812754700', N'abc@gmail.com', N'www.vxv.com', N'abc       ', N'01812754700', N'sdsd')
SET IDENTITY_INSERT [dbo].[Groups] OFF
SET IDENTITY_INSERT [dbo].[Items] ON 

INSERT [dbo].[Items] ([ItemId], [ItemName], [BrandName], [Unit], [BuyRate], [SaleRate], [Qty], [Discount], [PCQty], [CategoryId]) VALUES (21, N'TV', N'Sony', 1, 10000, 12000, 1, 0, 1, 35)
INSERT [dbo].[Items] ([ItemId], [ItemName], [BrandName], [Unit], [BuyRate], [SaleRate], [Qty], [Discount], [PCQty], [CategoryId]) VALUES (23, N'Laptop', N'Dell', 1, 40000, 45000, 1, 1, 1, 35)
INSERT [dbo].[Items] ([ItemId], [ItemName], [BrandName], [Unit], [BuyRate], [SaleRate], [Qty], [Discount], [PCQty], [CategoryId]) VALUES (24, N'Cake', N'Pran', 1, 50, 60, 100, 0, 10, 67)
INSERT [dbo].[Items] ([ItemId], [ItemName], [BrandName], [Unit], [BuyRate], [SaleRate], [Qty], [Discount], [PCQty], [CategoryId]) VALUES (25, N'Juice', N'Pran', 1, 20, 25, 10, 0, 10, 67)
INSERT [dbo].[Items] ([ItemId], [ItemName], [BrandName], [Unit], [BuyRate], [SaleRate], [Qty], [Discount], [PCQty], [CategoryId]) VALUES (26, N'Hair Oil', N'Uniliver', 1, 50, 60, 2, 0, 10, 69)
INSERT [dbo].[Items] ([ItemId], [ItemName], [BrandName], [Unit], [BuyRate], [SaleRate], [Qty], [Discount], [PCQty], [CategoryId]) VALUES (27, N'Hair Cream', N'Uniliver', 1, 150, 280, 1, 0, 10, 68)
INSERT [dbo].[Items] ([ItemId], [ItemName], [BrandName], [Unit], [BuyRate], [SaleRate], [Qty], [Discount], [PCQty], [CategoryId]) VALUES (28, N'Reading Table', N'Partex', 1, 10000, 13000, 1, 0, 1, 70)
INSERT [dbo].[Items] ([ItemId], [ItemName], [BrandName], [Unit], [BuyRate], [SaleRate], [Qty], [Discount], [PCQty], [CategoryId]) VALUES (29, N'Burger', N'Well Food', 1, 50, 60, 2, 0, 0, 67)
INSERT [dbo].[Items] ([ItemId], [ItemName], [BrandName], [Unit], [BuyRate], [SaleRate], [Qty], [Discount], [PCQty], [CategoryId]) VALUES (30, N'Desktop', N'HP', 1, 15000, 20000, 1, 0, 1, 35)
INSERT [dbo].[Items] ([ItemId], [ItemName], [BrandName], [Unit], [BuyRate], [SaleRate], [Qty], [Discount], [PCQty], [CategoryId]) VALUES (31, N'Tablet', N'Sony', 1, 10000, 12000, 2, 0, 2, 35)
SET IDENTITY_INSERT [dbo].[Items] OFF
SET IDENTITY_INSERT [dbo].[MasterStocks] ON 

INSERT [dbo].[MasterStocks] ([Id], [Date], [MemoNo], [Year], [Type], [StoreId], [GroupId]) VALUES (10, CAST(N'2019-01-28' AS Date), 20190001, 2019, N'Purchase', 111, 1)
INSERT [dbo].[MasterStocks] ([Id], [Date], [MemoNo], [Year], [Type], [StoreId], [GroupId]) VALUES (1, CAST(N'2019-01-21' AS Date), 20190001, 2019, N'Sell', 1, 1)
INSERT [dbo].[MasterStocks] ([Id], [Date], [MemoNo], [Year], [Type], [StoreId], [GroupId]) VALUES (56, CAST(N'2019-01-30' AS Date), 20190002, 2019, N'Purchase', 241423123, 4)
INSERT [dbo].[MasterStocks] ([Id], [Date], [MemoNo], [Year], [Type], [StoreId], [GroupId]) VALUES (57, CAST(N'2019-01-30' AS Date), 20190003, 2019, N'Purchase', 111, 4)
INSERT [dbo].[MasterStocks] ([Id], [Date], [MemoNo], [Year], [Type], [StoreId], [GroupId]) VALUES (58, CAST(N'2019-01-30' AS Date), 20190004, 2019, N'Purchase', 211221221, 4)
SET IDENTITY_INSERT [dbo].[MasterStocks] OFF
INSERT [dbo].[Stocks] ([CategoryId], [ItemId], [Year], [MemoNo], [Type], [ItemSL], [Quantity], [Rate], [Amount], [Discount], [GrossAmount]) VALUES (35, 21, 2019, 20190001, N'Sell', 1, 1, 12000, 1, 0, 12000)
INSERT [dbo].[Stocks] ([CategoryId], [ItemId], [Year], [MemoNo], [Type], [ItemSL], [Quantity], [Rate], [Amount], [Discount], [GrossAmount]) VALUES (67, 25, 2019, 20190002, N'Purchase', 1, 2, 25, 50, 0, 50)
INSERT [dbo].[Stocks] ([CategoryId], [ItemId], [Year], [MemoNo], [Type], [ItemSL], [Quantity], [Rate], [Amount], [Discount], [GrossAmount]) VALUES (69, 26, 2019, 20190004, N'Purchase', 1, 2, 60, 120, 0, 120)
SET IDENTITY_INSERT [dbo].[Stores] ON 

INSERT [dbo].[Stores] ([Id], [StoreName], [StoreId], [Remark]) VALUES (20, N'Mohammad', 111, N'abc')
INSERT [dbo].[Stores] ([Id], [StoreName], [StoreId], [Remark]) VALUES (18, N'Karim Store', 124, N'cxvxcv')
INSERT [dbo].[Stores] ([Id], [StoreName], [StoreId], [Remark]) VALUES (19, N'Rahman', 241423123, N'sdljsdofjsdafas')
INSERT [dbo].[Stores] ([Id], [StoreName], [StoreId], [Remark]) VALUES (24, N'lashdlh', 211221221, N'sdsdljs')
INSERT [dbo].[Stores] ([Id], [StoreName], [StoreId], [Remark]) VALUES (35, N'b', 1, N'abc')
SET IDENTITY_INSERT [dbo].[Stores] OFF
/****** Object:  Index [IX_Items]    Script Date: 1/30/2019 6:30:50 PM ******/
CREATE NONCLUSTERED INDEX [IX_Items] ON [dbo].[Items]
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Items]  WITH CHECK ADD  CONSTRAINT [FK_Items_Categories] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Categories] ([CategoryId])
GO
ALTER TABLE [dbo].[Items] CHECK CONSTRAINT [FK_Items_Categories]
GO
/****** Object:  StoredProcedure [dbo].[getCategory]    Script Date: 1/30/2019 6:30:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getCategory] AS SELECT * FROM Categories
GO
USE [master]
GO
ALTER DATABASE [InventoryManagementDB] SET  READ_WRITE 
GO
