﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InventoryManagementWebApp.Models
{
    public class Stock
    {
        public Int64 StockId { get; set; }
        public Int64 CategoryId { get; set; }
        public Int64 ItemId { get; set; }
        public int Year { get; set; }
        public int MemoNo { get; set; }
        public string Type { get; set; }
        public int ItemSL { get; set; }
        public int Quantity { get; set; }
        public int Rate { get; set; }
        public int Amount { get; set; }
        public int Discount { get; set; }
        public int GrossAmount { get; set; }
    }
}