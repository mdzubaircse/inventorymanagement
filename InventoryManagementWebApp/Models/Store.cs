﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InventoryManagementWebApp.Models
{
    public class Store
    {
        public Int64 Id { get; set; }
        public string StoreName { get; set; }
        public Int64 StoreId { get; set; }
        public string Remark { get; set; }
    }
}