﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InventoryManagementWebApp.Models
{
    public class Item
    {
        public Int64 ItemId { get; set; }
        public string ItemName { get; set; }
        public string BrandName { get; set; }
        public int Unit { get; set; }
        public int  BuyRate{ get; set; }
        public int SaleRate { get; set; }
        public int Qty { get; set; }
        public int Discount { get; set; }
        public int PCQty { get; set; }
        public Int64 CategoryId { get; set; }

    }
}