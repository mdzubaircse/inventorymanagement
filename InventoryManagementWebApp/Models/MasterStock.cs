﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InventoryManagementWebApp.Models
{
    public class MasterStock
    {
        public Int64 Id { get; set; }
        public DateTime Date { get; set; }
        public int Year { get; set; }
        public string Type { get; set; }
        public int MemoNo { get; set; }
        public Int64 StoreId { get; set; }
        public Int64 GroupId { get; set; }    // Used for SupplierId
    }
}