﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace InventoryManagementWebApp.Models
{
    public class Category
    {
        public Int64 CategoryId { get; set; }
        [Required]
        public string CategoryName { get; set; }
    }
}