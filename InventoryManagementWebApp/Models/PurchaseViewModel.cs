﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InventoryManagementWebApp.Models
{
    public class PurchaseViewModel
    {
        public int ItemSL { get; set; }
        public string ItemName { get; set; }
        public int Quantity { get; set; }
        public int Amount { get; set; }
        public int Discount { get; set; }
        public int GrossAmount { get; set; }
        public int CategoryId { get; set; }
    }
}