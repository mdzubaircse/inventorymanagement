﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InventoryManagementWebApp.Models
{
    public class Group
    {
        public Int64 GroupId { get; set; }
        public string GroupNm { get; set; }
        public string PartyNm { get; set; }
        public string Address { get; set; }
        public string TelephoneNo { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string WebAddress { get; set; }
        public string ContactPersonNm { get; set; }
        public string ContactMobileNo { get; set; }
        public string Remark { get; set; }
    }
}