﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(InventoryManagementWebApp.Startup))]
namespace InventoryManagementWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
