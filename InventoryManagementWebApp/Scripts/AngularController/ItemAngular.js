﻿
var app = angular.module("itemApp", [])
.controller("ItemController", function ($scope, $http) {
    $scope.SaveItem = function () {
        var item = {
            ItemName: $("#txtItemNm").val(),
            BrandName: $("#txtBrandNm").val(),
            Unit: $("#txtUnit").val(),
            BuyRate: $("#txtBuyRt").val(),
            SaleRate: $("#txtSaleRt").val(),
            Qty: $("#txtQty").val(),
            Discount: $("#txtDiscount").val(),
            PCQty: $("#txtPCQty").val()
        }
        $http.post('/Item/InsertItem', item
            ).success(function (data) {

                $scope.message = data;
            })
         .error(function (data, status, header, config) {

             $scope.message = "Save Failed!";
         });
    }
});