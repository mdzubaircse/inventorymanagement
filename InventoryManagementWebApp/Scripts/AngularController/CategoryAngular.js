﻿
var app = angular.module("app", []);

   app.controller("CategoryController", function ($scope,$http) {
       $scope.SaveData = function() {
               var category = {
                   CategoryName : $("#txtCategoryNm").val()
           }
           $http.post('/Category/InsertData',category
               ).success(function (data) {
                   
                   $scope.message = data;
               })
            .error(function (data, status, header, config) {
               
                $scope.message = "Save Failed!";
            });
           
       }

       $scope.SaveItem = function () {
           var item = {
               ItemName: $("#txtItemNm").val(),
               BrandName: $("#txtBrandNm").val(),
               Unit: $("#txtUnit").val(),
               BuyRate: $("#txtBuyRt").val(),
               SaleRate: $("#txtSaleRt").val(),
               Qty: $("#txtQty").val(),
               Discount: $("#txtDiscount").val(),
               PCQty: $("#txtPCQty").val()
           }
           $http.post('/Item/InsertItem', item
               ).success(function (data) {

                   $scope.message = data;
               })
            .error(function (data, status, header, config) {

                $scope.message = "Save Failed!";
            });
       }








   });

//var apps = angular.module("itemApp", []);
//   apps.controller("ItemController", function ($scope, $http) {
//       $scope.SaveItem = function () {
//           var item = {
//               ItemName: $("#txtItemNm").val(),
//               BrandName: $("#txtBrandNm").val(),
//               Unit: $("#txtUnit").val(),
//               BuyRate: $("#txtBuyRt").val(),
//               SaleRate: $("#txtSaleRt").val(),
//               Qty: $("#txtQty").val(),
//               Discount: $("#txtDiscount").val(),
//               PCQty: $("#txtPCQty").val()
//           }
//           $http.post('/Item/InsertItem', item
//               ).success(function (data) {

//                   $scope.message = data;
//               })
//            .error(function (data, status, header, config) {

//                $scope.message = "Save Failed!";
//            });
//       }
//   });