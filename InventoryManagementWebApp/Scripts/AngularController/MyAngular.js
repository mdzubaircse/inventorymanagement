﻿
var app = angular.module("app", []);

app.controller("CategoryController", function ($scope, $http) {

    //save category in database
       $scope.SaveData = function() {
               var category = {
                   CategoryName : $("#txtCategoryNm").val()
           }
           $http.post('/Category/InsertData',category
               ).success(function (data) {
                   if (data === "Success") {
                       //alert("Form Load");
                       $scope.message = "Save Successfull";
                       $scope.ShowItemEntryForm();
                       $scope.IsVisibleItemTable = false;
                       $scope.category.CategoryName = ""; ///this line is used for removing value from categoryName so that the table can't be loaded when new category is inserted
                   } else if (data === "Exist") {
                       $scope.message = "Exist Category";
                       $scope.LoadData(category.CategoryName);
                       $scope.IsVisibleForm = true;
                       $scope.ShowItemTable();
                       $scope.GetCategory();
                       //alert("Table Load");
                   } else {
                       alert("Failed");
                   }
                   //$scope.GetCategory();
               })
            .error(function (data, status, header, config) {
               
                $scope.message = "Save Failed!";
            });
         
       }

       //sava item in database

       $scope.SaveItem = function () {
           var item = {
               ItemName: $("#txtItemNm").val(),
               BrandName: $("#txtBrandNm").val(),
               Unit: $("#txtUnit").val(),
               BuyRate: $("#txtBuyRt").val(),
               SaleRate: $("#txtSaleRt").val(),
               Qty: $("#txtQty").val(),
               Discount: $("#txtDiscount").val(),
               PCQty: $("#txtPCQty").val(),
               CategoryId:$scope.catId
           }
           $http.post('/Item/InsertItem', item
               ).success(function (data) {
                   $scope.message = data;
                   $scope.LoadData();///to load data after new item saved
                   $scope.item = {};
               })
            .error(function (data, status, header, config) {

                $scope.message = "Save Failed!";
            });
       }

       //This function is used to visible Item entry form
       $scope.IsVisibleForm = false;
       $scope.ShowItemEntryForm = function () {
           $scope.IsVisibleForm = true;
       }

       //This function is used to visible Item Table
       $scope.IsVisibleItemTable = false;
       $scope.ShowItemTable = function () {
           $scope.IsVisibleItemTable = true;
       }



       //To load data from database in a table

       $scope.LoadData = function (x) {

           var category = {
               CategoryName: $scope.category.CategoryName
           }
           $http.post('/Item/GetAllItem', category
               ).success(function (data) {
                   $scope.IsVisibleItemTable = false;
                   $scope.AllItem = data;
                   $scope.IsVisibleItemTable = true;

                   //$scope.display = true;
               })
            .error(function (data, status, header, config) {
            });
       }


       $scope.GetCategory = function () {

           var category = {
               CategoryName: $scope.category.CategoryName
           }
           $http.post('/Category/GetCategory', category
               ).success(function (data) {
                   $scope.IsVisibleItemTable = false;
                   $scope.catId = data;
                   $scope.IsVisibleItemTable = true;

                   //$scope.display = true;
               })
            .error(function (data, status, header, config) {
            });
       }










    //This method is used for update item


       $scope.UpdateItem = function (x) {
           var items = {
              ItemId:x.ItemId,
              ItemName: x.ItemName,
              BrandName: x.BrandName,
              Unit: x.Unit,
              BuyRate: x.BuyRate,
              SaleRate: x.SaleRate,
              Qty: x.Qty,
              Discount: x.Discount,
              PCQty: x.PCQty
          }
           $http.post('/Item/UpdateItem', items
               ).success(function (data) {
                   $scope.message = "";
                   $scope.message = data;
                   //alert(data);
                   
               })
            .error(function (data, status, header, config) {

                $scope.message = "Save Failed!";
            });
       }



    $scope.UpdateCancel = function() {
        $scope.LoadData();///to load data after new item saved
    }




    //This is method is used for updating purchase information
    $scope.UpdatePurchaseInfo = function (x, z) {

        var s = this.x;

        console.log(s);
        //var stock = {
        //    MemoNo: $('#txtMemoNo').val(),
        //    ItemId: $('#ddlItemNmE').val(),
        //    Type: "Purchase",
        //    ItemSL: x.ItemSL,
        //    Quantity: x.Quantity,
        //    Amount: $('#txtAmountT').val(),
        //    Discount: x.Discount,
        //    GrossAmount: $('#txtGrossAmountT').val()
        //}
        //$http.post('/Stock/UpdateStock', stock
        //    ).success(function (data) {
        //        $scope.LoadPurchaseTable(x);
        //        //alert(data);

        //    })
        // .error(function (data, status, header, config) {

        //     $scope.message = "Update Failed!";
        // });
    }

    //load purchase table after edit data from table
    $scope.LoadPurchaseTable = function (x) {
        var stock = {
            MemoNo: $('#txtMemoNo').val(),
            Type: "Purchase",
            ItemSL: x.ItemSL,
            Quantity: x.Quantity,
            Amount: $('#txtAmountT').val(),
            Discount: x.Discount,
            GrossAmount: $('#txtGrossAmountT').val()
        }
        $http.post('/Stock/UpdateTableInfo', stock
            ).success(function (data) {

                $scope.IsVisibleItemTable = false;
                $scope.PurchaseInfo = data;
                $scope.IsVisibleItemTable = true;
                
                //alert("Updated data");

            })
         .error(function (data, status, header, config) {

             $scope.message = "Update Failed!";
         });
    }
    

    //
    /// This method is used for deleting data from purchase table
    //


    $scope.DeletePurchaseInfo = function (x) {
        var isDelete = confirm("Do you want to delete this item?");
        if (isDelete == true) {
            var purchaseItem = {
                MemoNo: $('#txtMemoNo').val(),
                Type: "Purchase",
                ItemSL: x.ItemSL
            }
            $http.post('/Stock/DeleteItem', purchaseItem).success(function (data) {
                //$scope.LoadPurchaseTable(x);///to load data after new item saved
                    //alert(data);
                })
             .error(function () {
                 $scope.message = "Delete Failed!";
             });
        }
    }







//this method is used for deleting item from item table

    $scope.DeleteItem = function (x) {
        var isDelete = confirm("Do you want to delete this item?");
        if (isDelete == true) {
            var items = {
                ItemId: x.ItemId
            }
            $http.post('/Item/DeleteItem', items).success(function (data) {
                $scope.LoadData();///to load data after new item saved
                $scope.message = data;
                //alert(data);
            })
             .error(function () {
                 $scope.message = "Delete Failed!";
             });
        }   
       }


    // This method is used for toggoling table to Form and from to table
       $scope.EditData = function(x) {
           x.display = x.display ? false : true;
           //alert(x.display);
       }
      




    //Save group info

       $scope.SaveGroup = function () {
           //alert($scope.group.GroupNm);
           var group = {
               GroupNm: $scope.group.GroupNm,
               PartyNm: $scope.group.PartyNm,
               Address: $scope.group.Address,
               TelephoneNo: $scope.group.TelephoneNo,
               MobileNo: $scope.group.MobileNo,
               Email: $scope.group.Email,
               WebAddress: $scope.group.WebAddress,
               ContactPersonNm: $scope.group.ContactPersonNm,
               ContactMobileNo: $scope.group.ContactMobileNo,
               Remark: $scope.group.Remark
           }
           $http.post('/Group/SaveGroupInfo', group).success(function (data) {
               $scope.message = data;
               alert(data);

           }).error(function () {
               $scope.message = "Group Information Save Failed!";
           });
       }

    //save store information

       $scope.SaveStore = function () {
           
           var store = {
               StoreName: $scope.store.StoreName,
               StoreId: $scope.store.StoreId,
               Remark: $scope.store.Remark
           }
           $http.post('/Store/SaveStoreInfo', store).success(function (data) {
               $scope.message = data;
               $scope.LoadStoreInfos(); //load store table after clicking
               alert(data);

           }).error(function () {
               $scope.message = "Store Information Save Failed!";
           });
       }




    //Load store Infromation

       $scope.LoadStoreInfos = function () {

           $http.post('/Store/GetStores'
               ).success(function (data) {
                   
                   $scope.StoreInfos = data;
                   $scope.ShowItemTable();
               })
            .error(function (data, status, header, config) {
            });
       }




       $scope.DeleteStoreInfo = function (x) {
           var isDelete = confirm("Do you want to delete this Info?");
           if (isDelete === true) {
               var sInfo = {
                   Id: x.Id
               }
               $http.post('/Store/DeleteStoreInfo', sInfo).success(function (data) {
                   $scope.LoadStoreInfo();///to load data after new item saved
                   $scope.message = data;
                   $scope.LoadStoreInfos();
                   //alert(data);
               })
                .error(function () {
                    $scope.message = "Delete Failed!";
                });
           }
       }




    ///Update store info

       $scope.UpdateStoreInfo = function (x) {
           var iInfo = {
               Id:x.Id,
               StoreName: x.StoreName,
               StoreId: x.StoreId,
               Remark: x.Remark
           }
           $http.post('/Store/UpdateStore', iInfo
               ).success(function (data) {
                   alert(data);
                   $scope.message = data;
                   //alert(data);
                   $scope.LoadStoreInfo();///to load data after new item saved
               })
            .error(function (data, status, header, config) {

                $scope.message = "Save Failed!";
            });
       }


       $scope.StoreUpdateCancel = function () {
           $scope.LoadStoreInfo();///to load data after new item saved
       }





    ///purchase information creation

    //get store name and id
       $scope.LoadStoreInfo = function (x) {
           $http.post('/MasterStock/GetAllStore'
               ).success(function (data) {
                   //alert("Load all store info");
                   $scope.AllStoreInfo = data;
               })
            .error(function (data, status, header, config) {
            });
       }



    //get store name and id
       $scope.LoadSupplierInfo = function () {
           $http.post('/MasterStock/GetAllSupplier'
               ).success(function (data) {
                   //alert("Load all store info");
                   $scope.AllSupplierInfo = data;
               })
            .error(function (data, status, header, config) {
            });
       }



    //get year from date

       $scope.GetYear = function (s) {
           //alert($scope.s.date);
           $scope.SelectedYear = $scope.s.date;
           var y = $scope.SelectedYear.getFullYear();
           //alert(y);
           $scope.GenerateMemoForPurchase(y);
           //$scope.GenerateMemoForSale(y);
           
       }


    // Generate memo number for purchase

       $scope.GenerateMemoForPurchase = function (year) {
           var nYear = {
               MemoNo: year
           }
           $http.post('/MasterStock/GenerateMemoNoForPurchase', nYear
               ).success(function (data) {
                   //alert(data);
                   $scope.MemoNo = data;
               })
            .error(function () {
            });
       }

    // Generate memo number for sale

       $scope.GenerateMemoForSale = function (year) {
           var nYear = {
               MemoNo: year
           }
           $http.post('/MasterStock/GenerateMemoNoForSale', nYear
               ).success(function (data) {
                   //alert(data);
                   $scope.MemoNo = data;
               })
            .error(function () {
            });
       }


       $scope.LoadCategory = function () {
           $http.post('/Stock/GetCategories'
               ).success(function (data) {
                   //alert("Load all store info");
                   $scope.AllCategory = data;
               })
            .error(function (data, status, header, config) {
            });
       }


    //Load Item Information accorrding to category
       $scope.LoadItemInfo = function () {
           $http.post('/Stock/GetCategories'
               ).success(function (data) {
                   //alert("Load all store info");
                   $scope.AllCategory = data;
               })
            .error(function (data, status, header, config) {
            });
       }

       $scope.LoadItemInfoByCatId = function () {
           //alert($scope.x.CategoryId);
           var itm = {
               CategoryId: $scope.x.CategoryId
           }
           $http.post('/Stock/GetItemByCatId', itm
               ).success(function (data) {
                   $scope.Items = data;
               })
            .error(function (data, status, header, config) {
            });
       }


    //get item information by Id
       $scope.LoadItemInfoByItemId = function () {
           //alert($scope.x.CategoryId);
           var itemInfo = {
               ItemId: $scope.x.ItemId
           }
           $http.post('/Stock/GetItemInfoById', itemInfo
               ).success(function (data) {
                   $scope.ItemInfos = data;
                   //alert($scope.Items.SaleRate + " " + $scope.Items.Discount);
               })
            .error(function (data, status, header, config) {
            });
       }

    // save data in master stock

       $scope.SaveItemDataInMStock = function () {
           var mStock = {
               Date: $scope.s.date,
               MemoNo: $scope.MemoNo,
               Year: $scope.SelectedYear.getFullYear(),
               Type: "Purchase",
               StoreId: $scope.x.StoreId,
               GroupId: $scope.y.GroupId
           }
           $http.post('/MasterStock/SaveInMStockInfo', mStock
               ).success(function (data) {
                   $scope.Msg = data;
                   //alert($scope.Msg);
               })
            .error(function (data, status, header, config) {
            });
       }




    // save data in stock

       $scope.SaveItemDataInStock = function () {
           var stock = {
               CategoryId: $scope.x.CategoryId,
               ItemId: $scope.x.ItemId,
               Year: $scope.SelectedYear.getFullYear(),
               MemoNo: $scope.MemoNo,
               Type: "Purchase",
               ItemSL: $scope.x.StoreId,
               Quantity: $scope.Quantity,
               Rate: $scope.ItemInfos.SaleRate,
               Amount: $('#txtAmount').val(),
               DiscountParcent: $scope.DiscountParcent,
               Discount: $('#txtDiscount').val(),
               GrossAmount: $('#txtGrossAmount').val()
           }
           $http.post('/Stock/PurchaseEntry', stock
               ).success(function (data) {
                   $scope.PurchaseInfo = data;
                   $scope.SaveItemDataInMStock(); ///This method is used for memonumber saving in master stock table
                   $scope.ShowItemTable(); //it is used for showing table after add button click
                   
               })
            .error(function (data, status, header, config) {
            });
       }

       

       $scope.ClearPurchaseFormData = function() {
           $scope.Quantity = 0;
           $scope.ItemInfos.SaleRate = 0;
           $('#txtAmount').val(0);
           $scope.DiscountParcent = 0;
           $('#txtDiscount').val(0);
           $('#txtGrossAmount').val(0);
       }
       

    //Save stock for sale

       $scope.SaveItemDataInStockForSale = function () {
           var stock = {
               CategoryId: $scope.x.CategoryId,
               ItemId: $scope.x.ItemId,
               Year: $scope.SelectedYear.getFullYear(),
               MemoNo: $scope.MemoNo,
               Type: "Sell",
               ItemSL: $scope.x.StoreId,
               Quantity: $scope.Quantity,
               Rate: $scope.ItemInfos.SaleRate,
               Amount: $('#txtAmount').val(),
               DiscountParcent: $scope.DiscountParcent,
               Discount: $('#txtDiscount').val(),
               GrossAmount: $('#txtGrossAmount').val()
           }
           $http.post('/Stock/SaleEntry', stock
               ).success(function (data) {
                   $scope.SaleInfo = data;

                   $scope.ShowItemTable(); //it is used for showing table after add button click
                   //alert($scope.PurchaseInfo);
               })
            .error(function (data, status, header, config) {
            });
       }


    //Total amount count
       $scope.totalAmountForPurchase = function () {
           var total = 0;
           for (var count = 0; count < $scope.PurchaseInfo.length; count++) {
               total += parseFloat($scope.PurchaseInfo[count].GrossAmount, 10);
           }
           return total;
       };


       $scope.totalAmountForSale = function () {
           var total = 0;
           for (var count = 0; count < $scope.SaleInfo.length; count++) {
               total += parseFloat($scope.SaleInfo[count].GrossAmount, 10);
           }
           return total;
       };

    //Here, 

       $scope.deleteMemoNo = function () {
           var masterStock = {
               MemoNo: $scope.MemoNo
           }

        if ($scope.PurchaseInfo.length === 1) {
            
            $http.post('/MasterStock/DeleteMemoFromMasterStock', masterStock
               ).success(function (data) {
                    alert(data);

                })
            .error(function (data, status, header, config) {
            });

        }
    }




       $scope.LoadItemInfoForUpdateInPurchaseTbl = function () {
           //alert($scope.x.CategoryId);
           var itemInfo = {
               ItemId: $('#ddlItemNmE').val()
           }
           //alert($('#ddlItemNmE').val());
           $http.post('/Stock/GetItemInfoById', itemInfo
               ).success(function (data) {
                   $('#txtQuantity').val(0);
                   $('#txtAmountT').val(data.SaleRate);
                   $('#txtDiscount').val(0);
                   $('#txtGrossAmountT').val(data.SaleRate);

               })
            .error(function (data, status, header, config) {
            });
       }






















   });


