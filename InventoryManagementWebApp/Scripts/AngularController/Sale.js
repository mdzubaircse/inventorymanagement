﻿var app = angular.module("app", []);

app.controller("SaleController", function ($scope, $http) {

    //get year from date

       $scope.GetYear = function (s) {
          
           $scope.SelectedYear = $scope.s.date;
           var y = $scope.SelectedYear.getFullYear();
          
           $scope.GenerateMemoForSale(y);
          
           
       }




    // Generate memo number for purchase

       $scope.GenerateMemoForSale = function (year) {
           var nYear = {
               MemoNo: year
           }
           $http.post('/MasterStock/GenerateMemoNoForSale', nYear
               ).success(function (data) {
                  
                   $scope.MemoNo = data;
               })
            .error(function () {
            });
       }





    //Load category
       $scope.LoadCategory = function () {
           $http.post('/Stock/GetCategories'
               ).success(function (data) {
                   //alert("Load all store info");
                   $scope.AllCategory = data;
               })
            .error(function (data, status, header, config) {
            });
       }



    //get store name and id
       $scope.LoadStoreInfo = function (x) {
           $http.post('/MasterStock/GetAllStore'
               ).success(function (data) {
                   //alert("Load all store info");
                   $scope.AllStoreInfo = data;
               })
            .error(function (data, status, header, config) {
            });
       }



    //get supplier name and id
       $scope.LoadSupplierInfo = function () {
           $http.post('/MasterStock/GetAllParty'
               ).success(function (data) {
                   //alert("Load all store info");
                   $scope.AllSupplierInfo = data;
               })
            .error(function (data, status, header, config) {
            });
       }



    //Load item info by category Id
       $scope.LoadItemInfoByCatId = function () {
           //alert($scope.x.CategoryId);
           var itm = {
               CategoryId: $scope.c.CategoryId
           }
           $http.post('/Stock/GetItemByCatId', itm
               ).success(function (data) {
                   $scope.Items = data;
               })
            .error(function (data, status, header, config) {
            });
       }


    //get item information by Id
       $scope.LoadItemInfoByItemId = function () {
           //alert($scope.x.CategoryId);
           var itemInfo = {
               ItemId: $scope.i.ItemId
               //ItemId: $('#ddlItemFromStock').val()
       }
           $http.post('/Stock/GetItemInfoById', itemInfo
               ).success(function (data) {
                   $scope.ItemInfos = data;
                   //alert($scope.Items.SaleRate + " " + $scope.Items.Discount);
               })
            .error(function (data, status, header, config) {
            });
       }


    // save data in stock

       $scope.SaveItemDataInStock = function () {
           var stock = {
               CategoryId: $scope.c.CategoryId,
               ItemId: $scope.i.ItemId,
               Year: $scope.SelectedYear.getFullYear(),
               MemoNo: $scope.MemoNo,
               Type: "Sell",
               ItemSL: $scope.s.StoreId,
               Quantity: $scope.Quantity,
               Rate: $scope.ItemInfos.SaleRate,
               Amount: $('#txtAmount').val(),
               DiscountParcent: $scope.DiscountParcent,
               Discount: $('#txtDiscount').val(),
               GrossAmount: $('#txtGrossAmount').val()
           }
           $http.post('/Stock/SaleEntry', stock
               ).success(function (data) {
                   $scope.PurchaseInfo = data;
                   $scope.SaveItemDataInMStock(); ///This method is used for memonumber saving in master stock table
                   $scope.ShowItemTable(); //it is used for showing table after add button click

               })
            .error(function (data, status, header, config) {
            });
       }




    // save data in master stock

       $scope.SaveItemDataInMStock = function () {
           var mStock = {
               Date: $scope.s.date,
               MemoNo: $scope.MemoNo,
               Year: $scope.SelectedYear.getFullYear(),
               Type: "Sell",
               StoreId: $scope.s.StoreId,
               GroupId: $scope.y.GroupId
           }
           $http.post('/MasterStock/SaveInMStockInfo', mStock
               ).success(function (data) {
                   $scope.Msg = data;
                   //alert($scope.Msg);
               })
            .error(function (data, status, header, config) {
            });
       }








       $scope.LoadItemInfoForUpdateInPurchaseTbl = function (x) {
           //alert($scope.x.CategoryId);
           var a = 0;
           var itemInfo = {
               ItemId: x.ItemId
           }
           //alert($('#ddlItemNmE').val());
           $http.post('/Stock/GetItemInfoById', itemInfo
               ).success(function (data) {
                  // alert("Sale Rate : " + data.SaleRate);
                   a = data.SaleRate;


               })
            .error(function (data, status, header, config) {
            });
           this.x.Amount = a;
       }


    //load purchase table after edit data from table
       $scope.LoadPurchaseTable = function (x) {
           var stock = {
               MemoNo: $('#txtMemoNo').val()
               
           }
           $http.post('/Stock/UpdateTableInfo', stock
               ).success(function (data) {

                   $scope.IsVisibleItemTable = false;
                   $scope.PurchaseInfo = data;
                   $scope.IsVisibleItemTable = true;

                   //alert("Updated data");

               })
            .error(function (data, status, header, config) {

                $scope.message = "Update Failed!";
            });
       }


    // This method is used for toggoling table to Form and from to table
       $scope.EditData = function (x) {
           x.display = x.display ? false : true;
           //alert(x.display);
       }


    //This is method is used for updating purchase information
       $scope.UpdatePurchaseInfo = function (x) {

           var s = this.x;

           console.log(s);
           var stock = {
               MemoNo: $('#txtMemoNo').val(),
               ItemId: x.ItemId,
               Type: "Sell",
               ItemSL: x.ItemSL,
               Quantity: x.Quantity,
               Amount: $('#txtAmountT').val(),
               Discount: x.Discount,
               GrossAmount: $('#txtGrossAmountT').val()
           }
           $http.post('/Stock/UpdateStock', stock
               ).success(function (data) {

                   //alert(data);
                   $scope.LoadPurchaseTable(); //This method is used for updating purchase table data after clicking Update button
               })
            .error(function (data, status, header, config) {

                $scope.message = "Update Failed!";
            });
       }


       $scope.CalculateGrossAmountByQuantity = function(x) {
           //alert(this.x.Quantity);
           this.x.GrossAmount = (this.x.Amount * this.x.Quantity)-this.x.Discount;
           //alert(this.x.GrossAmount);
       }

       $scope.CalculateGrossAmountByDiscount = function (x) {
           //alert(this.x.Quantity);
           this.x.GrossAmount = (this.x.Amount * this.x.Quantity) - this.x.Discount;
           //alert(this.x.GrossAmount);
       }











       $scope.StoreUpdateCancel = function () {
           $scope.LoadStoreInfo();///to load data after new item saved
       }


  
    /// This method is used for deleting data from purchase table
   
       $scope.DeletePurchaseInfo = function (x) {
           var isDelete = confirm("Do you want to delete this item?");
           if (isDelete == true) {
               var purchaseItem = {
                   MemoNo: $('#txtMemoNo').val(),
                   Type: "Sell",
                   ItemSL: x.ItemSL
               }
               $http.post('/Stock/DeleteItem', purchaseItem).success(function (data) {
                   //$scope.LoadPurchaseTable(x);///to load data after new item saved
                   //alert(data);
               })
                .error(function () {
                    $scope.message = "Delete Failed!";
                });
           }
       }


    //Delete memo number from master stock table
       $scope.deleteMemoNo = function () {
           var masterStock = {
               MemoNo: $scope.MemoNo
           }

           if ($scope.PurchaseInfo.length === 1) {

               $http.post('/MasterStock/DeleteMemoFromMasterStock', masterStock
                  ).success(function (data) {
                      alert(data);

                  })
               .error(function (data, status, header, config) {
               });

           }
       }


    //Total amount count
       $scope.totalAmountForPurchase = function () {
           var total = 0;
           for (var count = 0; count < $scope.PurchaseInfo.length; count++) {
               total += parseFloat($scope.PurchaseInfo[count].GrossAmount, 10);
           }
           return total;
       };


    //This function is used to visible Item entry form
       $scope.IsVisibleForm = false;
       $scope.ShowItemEntryForm = function () {
           $scope.IsVisibleForm = true;
       }

    //This function is used to visible Item Table
       $scope.IsVisibleItemTable = false;
       $scope.ShowItemTable = function () {
           $scope.IsVisibleItemTable = true;
       }







});