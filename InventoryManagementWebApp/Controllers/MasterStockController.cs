﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using InventoryManagementWebApp.Models;

namespace InventoryManagementWebApp.Controllers
{
    public class MasterStockController : Controller
    {
        public SqlConnection Connection { get; set; }
        public SqlCommand Command { get; set; }
        public SqlDataReader Reader { get; set; }

        public MasterStockController()
        {
            string connectionString = WebConfigurationManager.ConnectionStrings["InventoryContextDB"].ConnectionString;
            Connection = new SqlConnection(connectionString);
        }

        // GET: MasterStock
        public JsonResult GetAllStore()
        {
            return Json(GetStoreInfo(), JsonRequestBehavior.AllowGet);

        }


        public JsonResult GetAllSupplier()
        {
            return Json(GetSupplierInfo(), JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetAllParty()
        {
            return Json(GetPartyInfo(), JsonRequestBehavior.AllowGet);

        }

        public JsonResult GenerateMemoNoForPurchase(MasterStock nYear)
        {
            string y = nYear.MemoNo.ToString();
            return Json(GetMemoNoForPurchase(y), JsonRequestBehavior.AllowGet);
        }


        public JsonResult GenerateMemoNoForSale(MasterStock nYear)
        {
            string y = nYear.MemoNo.ToString();
            return Json(GetMemoNoForSale(y), JsonRequestBehavior.AllowGet);
        }


        public JsonResult SaveInMStockInfo(MasterStock mStock)
        {
            if (IsMemoNumberExists(mStock))  //this condition is used for protecting recreate memo number for the same entry
            {
                return Json("Memo number exist", JsonRequestBehavior.AllowGet);
            }
            return Json(SaveInMStock(mStock), JsonRequestBehavior.AllowGet);
        }

        //Delete memo from master stock table
        public JsonResult DeleteMemoFromMasterStock(MasterStock masterStock)
        {
            return Json(DeleteMemoNo(masterStock), JsonRequestBehavior.AllowGet);
        }


        public List<Store> GetStoreInfo()
        {

            string query = "SELECT * FROM Stores";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            List<Store> storeList = new List<Store>();
            while (Reader.Read())
            {
                Store store = new Store();
                store.StoreId = Convert.ToInt32(Reader["StoreId"]);
                store.StoreName = Reader["StoreName"].ToString();
                storeList.Add(store);
            }

            Connection.Close();
            return storeList;

        }

        public List<Group> GetSupplierInfo()
        {

            string query = "SELECT * FROM Groups WHERE GroupNm='Supplier'";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            List<Group> groupList = new List<Group>();
            while (Reader.Read())
            {
                Group group = new Group();
                group.GroupId = Convert.ToInt32(Reader["GroupId"]);
                group.PartyNm = Reader["PartyNm"].ToString();
                groupList.Add(group);
            }

            Connection.Close();
            return groupList;

        }


        public List<Group> GetPartyInfo()
        {

            string query = "SELECT * FROM Groups WHERE GroupNm='Party'";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            List<Group> groupList = new List<Group>();
            while (Reader.Read())
            {
                Group group = new Group();
                group.GroupId = Convert.ToInt32(Reader["GroupId"]);
                group.PartyNm = Reader["PartyNm"].ToString();
                groupList.Add(group);
            }

            Connection.Close();
            return groupList;

        }


        public string GetMemoNoForPurchase(string year)
        {
            string query = "SELECT * FROM MasterStocks WHERE Type='Purchase' AND Year="+ Convert.ToInt32(year)+ "";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            int rowCount = 0;
            while (Reader.Read())
            {
                rowCount++;
            }
            rowCount += 1;
            string memo = String.Empty;
            if (rowCount>=0 && rowCount<=9)
            {
                memo = year + "000" + rowCount;
            }
            else if (rowCount >= 10 && rowCount <= 99)
            {
                memo = year + "00" + rowCount;
            }
            else if (rowCount >= 100 && rowCount <= 999)
            {
                memo = year + "0" + rowCount;
            }
            else
            {
                memo = "Error";
            }
            Connection.Close();
            return memo;
        }





        public string GetMemoNoForSale(string year)
        {
            string query = "SELECT * FROM MasterStocks WHERE Type='Sell' AND Year=" + Convert.ToInt32(year) + "";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            int rowCount = 0;
            while (Reader.Read())
            {
                rowCount++;
            }
            rowCount += 1;
            string memo = String.Empty;
            if (rowCount >= 0 && rowCount <= 9)
            {
                memo = year + "000" + rowCount;
            }
            else if (rowCount >= 10 && rowCount <= 99)
            {
                memo = year + "00" + rowCount;
            }
            else if (rowCount >= 100 && rowCount <= 999)
            {
                memo = year + "0" + rowCount;
            }
            else
            {
                memo = "Error";
            }
            Connection.Close();
            return memo;
        }


        public string SaveInMStock(MasterStock mStock)
        {
            string query = "INSERT INTO MasterStocks VALUES(@date,@memoNo,@year,@type,@storedId,@groupId)";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@date", mStock.Date);
            Command.Parameters.AddWithValue("@memoNo", mStock.MemoNo);
            Command.Parameters.AddWithValue("@year", mStock.Year);
            Command.Parameters.AddWithValue("@type", mStock.Type);
            Command.Parameters.AddWithValue("@storedId", mStock.StoreId);
            Command.Parameters.AddWithValue("@groupId", mStock.GroupId);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            if (rowAffect>0)
            {
                return "Save Successful!";
            }
            return "Save Failed!";
        }




        public bool IsMemoNumberExists(MasterStock mStock)
        {
            string query = "SELECT * FROM MasterStocks WHERE (MemoNo="+ mStock.MemoNo+ " AND Year="+mStock.Year+" AND Type='"+mStock.Type+"')";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            bool isExists = Reader.HasRows;
            Connection.Close();
            return isExists;
        }




        public string DeleteMemoNo(MasterStock masterStock)
        {
            string query = "DELETE FROM MasterStocks WHERE MemoNo =" + masterStock.MemoNo + "";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            if (rowAffect > 0)
            {
                return "Delete Successfull!";
            }
            else
            {
                return "Delete Failed";
            }
        }

    }
}