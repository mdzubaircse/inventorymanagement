﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Sql;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using InventoryManagementWebApp.Models;

namespace InventoryManagementWebApp.Controllers
{
    public class CategoryController : Controller
    {
        public SqlConnection Connection { get; set; }
        public SqlCommand Command { get; set; }
        public SqlDataReader Reader { get; set; }

        public CategoryController()
        {
            string connectionString = WebConfigurationManager.ConnectionStrings["InventoryContextDB"].ConnectionString;
            Connection = new SqlConnection(connectionString);
        }

        //
        // GET: /Category/
        public ActionResult SaveCategory()
        {
            
            return View();
        }
        


        public JsonResult InsertData(Category category)
        {
            string msg = String.Empty;
            if (IsCatNameExists(category.CategoryName))
            {
                msg = "Exist";
            }
            else
            {
                int rowAffect = Save(category);
                if (rowAffect > 0)
                {
                    msg = "Success";
                }
                else
                {
                    msg = "Failed";
                }
            }

            return Json(msg, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetCategory(Category category)
        {
            return Json(GetCategoryId(category), JsonRequestBehavior.AllowGet);
        }


        public int Save(Category category)
        {
            string query = "INSERT INTO Categories VALUES('" + category.CategoryName + "')";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }
        public bool IsCatNameExists(string categoryName)
        {
            string query = "SELECT * FROM Categories WHERE CategoryName ='" + categoryName + "'";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            bool isExists = Reader.HasRows;
            Connection.Close();
            return isExists;
        }


        public Int64 GetCategoryId(Category category)
        {
            string query = "SELECT CategoryId FROM Categories WHERE CategoryName='" + category.CategoryName + "'";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            Int64 catId = 0;
            if (Reader.Read())
            {
                catId = Convert.ToInt64(Reader["CategoryId"]);
            }   
            Connection.Close();
            return catId;

        }



    }
}