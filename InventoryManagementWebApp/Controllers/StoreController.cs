﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using InventoryManagementWebApp.Models;

namespace InventoryManagementWebApp.Controllers
{
    public class StoreController : Controller
    {

        public SqlConnection Connection { get; set; }
        public SqlCommand Command { get; set; }
        public SqlDataReader Reader { get; set; }

        public StoreController()
        {
            string connectionString = WebConfigurationManager.ConnectionStrings["InventoryContextDB"].ConnectionString;
            Connection = new SqlConnection(connectionString);
        }
        // GET: Store
        public ActionResult StoreInfo()
        {
            return View();
        }

        public JsonResult SaveStoreInfo(Store store)
        {
            int rowAffect = Save(store);
            string msg = String.Empty;
            if (rowAffect > 0)
            {
                msg = "Save Successful";
            }
            else
            {
                msg = "Save Failed";
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }




        public JsonResult GetStores()
        {
            return Json(GetAllStore(), JsonRequestBehavior.AllowGet);

        }


        public JsonResult DeleteStoreInfo(Store sInfo)
        {
            Int64 id = sInfo.Id;
            string result = DeleteStoreInfoById(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }





        public JsonResult UpdateStore(Store sInfo)
        {
            string result = UpdateStoreById(sInfo);
            return Json(result, JsonRequestBehavior.AllowGet);
        }














        public int Save(Store store)
        {
            string query = "INSERT INTO Stores VALUES(@storeName,@storeId,@remark)";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@storeName", store.StoreName);
            Command.Parameters.AddWithValue("@storeId", store.StoreId);
            Command.Parameters.AddWithValue("@remark", store.Remark);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }



        public List<Store> GetAllStore()
        {
            string query = "SELECT * FROM Stores ORDER BY StoreName ASC";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            List<Store> itemList = new List<Store>();
            while (Reader.Read())
            {
                Store store = new Store();
                store.Id = Convert.ToInt64(Reader["Id"]);
                store.StoreName = Reader["StoreName"].ToString();
                store.StoreId = Convert.ToInt64(Reader["StoreId"].ToString());
                store.Remark = Reader["Remark"].ToString();
                itemList.Add(store);
            }

            Connection.Close();
            return itemList;

        }

        public string DeleteStoreInfoById(Int64 id)
        {
            string query = "DELETE FROM Stores WHERE Id =" + id + "";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            if (rowAffect > 0)
            {
                return "Delete Successfull!";
            }
            else
            {
                return "Delete Failed";
            }
        }






        public string UpdateStoreById(Store store)
        {

            try
            {
                string query ="UPDATE Stores SET StoreName=@SName, StoreId=@SId, Remark=@SRemark WHERE Id =@id";
                Command = new SqlCommand(query, Connection);
                Command.Parameters.AddWithValue("@SName", store.StoreName);
                Command.Parameters.AddWithValue("@SId", store.StoreId);
                Command.Parameters.AddWithValue("@SRemark", store.Remark);
                Command.Parameters.AddWithValue("@id", store.Id);
                Connection.Open();
                int rowAffect = Command.ExecuteNonQuery();
                Connection.Close();
                if (rowAffect > 0)
                {
                    return "Update Successfull!";
                }
                else
                {
                    return "Update Failed";
                }
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

    }
}