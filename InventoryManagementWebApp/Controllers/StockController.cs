﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using InventoryManagementWebApp.Models;

namespace InventoryManagementWebApp.Controllers
{
    public class StockController : Controller
    {
        public SqlConnection Connection { get; set; }
        public SqlCommand Command { get; set; }
        public SqlDataReader Reader { get; set; }
        

        public StockController()
        {
            string connectionString = WebConfigurationManager.ConnectionStrings["InventoryContextDB"].ConnectionString;
            Connection = new SqlConnection(connectionString);
        }


        // GET: Stock
        public ActionResult Purchase()
        {
            return View();
        }
        public ActionResult PurchaseTest()
        {
            return View();
        }
        public ActionResult Sale()
        {
            return View();
        }



        public JsonResult GetCategories()
        {
            return Json(GetAllCategorieses(), JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetItemByCatId(Item itm)
        {
            return Json(GetItemInfo(itm.CategoryId.ToString()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetItemInfoById(Item itemInfo)
        {
            return Json(GetItemInfos(itemInfo.ItemId.ToString()), JsonRequestBehavior.AllowGet);
        }

        // for purchase entry

        public JsonResult PurchaseEntry(Stock stock)
        {
            string itemSl = CreateItemSLForPurchase(stock);
            stock.ItemSL = Convert.ToInt32(itemSl);
            //string msg = String.Empty;
            SaveInStock(stock);
            return Json(GetPurchaseInfo(stock), JsonRequestBehavior.AllowGet);
        }


        // for sale entry
        public JsonResult SaleEntry(Stock stock)
        {
            string itemSl = CreateItemSLForSale(stock);
            stock.ItemSL = Convert.ToInt32(itemSl);
            //string msg = String.Empty;
            SaveInStock(stock);
            return Json(GetSaleInfo(stock), JsonRequestBehavior.AllowGet);
        }


        public JsonResult UpdateStock(Stock stock)
        {
            
            string result = UpdatePurchaseInfo(stock);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult UpdateTableInfo(Stock stock)
        {
            return Json(GetPurchaseInfo(stock), JsonRequestBehavior.AllowGet);

        }



        public JsonResult DeleteItem(Stock purchaseItem)
        {
            return Json(DeletePurchaseItem(purchaseItem), JsonRequestBehavior.AllowGet);
        }


        public List<Category> GetAllCategorieses()
        {
            string query = "SELECT * FROM Categories ORDER BY CategoryName ASC";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            List<Category> categoryList = new List<Category>();
            while (Reader.Read())
            {
                Category aCategories = new Category();
                aCategories.CategoryId = Convert.ToInt32(Reader["CategoryId"]);
                aCategories.CategoryName = Reader["CategoryName"].ToString();
                categoryList.Add(aCategories);
            }
            Connection.Close();
            return categoryList;

        }

        public List<Item> GetItemInfo(string catId)
        {
            int cId = Convert.ToInt32(catId);
            //int cId = 35;
            string query = "SELECT * FROM Items WHERE CategoryId = "+ cId + "";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            List<Item> itemList = new List<Item>();
            while (Reader.Read())
            {
                Item item = new Item();
                item.ItemId = Convert.ToInt32(Reader["ItemId"]);
                item.ItemName = Reader["ItemName"].ToString();
                itemList.Add(item);
            }
            Connection.Close();
            return itemList;

        }


        public Item GetItemInfos(string itemInfo)
        {
            int iId = Convert.ToInt32(itemInfo);
            string query = "SELECT * FROM Items WHERE ItemId = " + iId + "";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            Item item = new Item();
            if (Reader.Read())
            {
                item.SaleRate = Convert.ToInt32(Reader["SaleRate"]);
            }
            Connection.Close();
            return item;

        }



            //this method is used for creating an ItemSL number in Purchase entry according to MemoNumber
        public string CreateItemSLForPurchase(Stock memoNo)
        {
            string query = "SELECT * FROM Stocks WHERE (Year="+memoNo.Year+" AND Type='Purchase' AND MemoNo ="+memoNo.MemoNo+" )";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            int rowCount = 0;
            while (Reader.Read())
            {
                rowCount++;
            }
            rowCount += 1;
            
            Connection.Close();
            return rowCount.ToString();
        }


        //this method is used for creating an ItemSL number in Sell Entry according to MemoNumber
        public string CreateItemSLForSale(Stock memoNo)
        {
            string query = "SELECT * FROM Stocks WHERE (Year=" + memoNo.Year + " AND Type='Sell' AND MemoNo =" + memoNo.MemoNo + " )";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            int rowCount = 0;
            while (Reader.Read())
            {
                rowCount++;
            }
            rowCount += 1;

            Connection.Close();
            return rowCount.ToString();
        }

        //This method is used for Purchase Entry Or Sell Entry
        public int SaveInStock(Stock stock)
        {
            string query = "INSERT INTO Stocks VALUES(@catId,@itemId,@year,@memoNo,@type,@itemSl,@quantity,@rate, @amount, @discount, @grossAmount)";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@catId", stock.CategoryId);
            Command.Parameters.AddWithValue("@itemId", stock.ItemId);
            Command.Parameters.AddWithValue("@year", stock.Year);
            Command.Parameters.AddWithValue("@memoNo", stock.MemoNo);
            Command.Parameters.AddWithValue("@type", stock.Type);
            Command.Parameters.AddWithValue("@itemSl", stock.ItemSL);
            Command.Parameters.AddWithValue("@quantity", stock.Quantity);
            Command.Parameters.AddWithValue("@rate", stock.Rate);
            Command.Parameters.AddWithValue("@amount", stock.Amount);
            Command.Parameters.AddWithValue("@discount", stock.Discount);
            Command.Parameters.AddWithValue("@grossAmount", stock.GrossAmount);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }



        //This method is used for Loading Purchase Item in the table after clicking the add button
        public List<PurchaseViewModel> GetPurchaseInfo(Stock stock)
        {
            string query = "SELECT * FROM Purchases WHERE (Type='Purchase' AND MemoNo =" + stock.MemoNo + " )";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            List<PurchaseViewModel> stockList = new List<PurchaseViewModel>();
            while (Reader.Read())
            {
                PurchaseViewModel purchaseViewModel = new PurchaseViewModel();
                purchaseViewModel.ItemSL = Convert.ToInt32(Reader["ItemSL"]);
                purchaseViewModel.CategoryId = Convert.ToInt32(Reader["CategoryId"]);
                purchaseViewModel.ItemName = Reader["ItemName"].ToString();
                purchaseViewModel.Quantity = Convert.ToInt32(Reader["Quantity"]);
                purchaseViewModel.Amount = Convert.ToInt32(Reader["Amount"]);
                purchaseViewModel.Discount = Convert.ToInt32(Reader["Discount"]);
                purchaseViewModel.GrossAmount = Convert.ToInt32(Reader["GrossAmount"]);
                stockList.Add(purchaseViewModel);
            }
            Connection.Close();
            return stockList;

        }



        public List<PurchaseViewModel> GetSaleInfo(Stock stock)
        {
            string query = "SELECT * FROM Purchases WHERE (Year=" + stock.Year + " AND Type='Sell' AND MemoNo =" + stock.MemoNo + " )";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            List<PurchaseViewModel> stockList = new List<PurchaseViewModel>();
            while (Reader.Read())
            {
                PurchaseViewModel purchaseViewModel = new PurchaseViewModel();
                purchaseViewModel.ItemSL = Convert.ToInt32(Reader["ItemSL"]);
                purchaseViewModel.ItemName = Reader["ItemName"].ToString();
                purchaseViewModel.Quantity = Convert.ToInt32(Reader["Quantity"]);
                purchaseViewModel.Amount = Convert.ToInt32(Reader["Amount"]);
                purchaseViewModel.Discount = Convert.ToInt32(Reader["Discount"]);
                purchaseViewModel.GrossAmount = Convert.ToInt32(Reader["GrossAmount"]);
                stockList.Add(purchaseViewModel);
            }
            Connection.Close();
            return stockList;

        }




        public string UpdatePurchaseInfo(Stock stock)
        {

            
            try
            {
                string query ="UPDATE Stocks SET ItemId=@itemId, Quantity=@quantity, Amount=@amount, Discount=@discount, GrossAmount=@grossAmount WHERE MemoNo =@memoNo AND Type=@type  AND ItemSL=@itemSl";
                Command = new SqlCommand(query, Connection);
                Command.Parameters.AddWithValue("@itemId", stock.ItemId);
                Command.Parameters.AddWithValue("@quantity", stock.Quantity);
                Command.Parameters.AddWithValue("@amount", stock.Amount);
                Command.Parameters.AddWithValue("@discount", stock.Discount);
                Command.Parameters.AddWithValue("@grossAmount", stock.GrossAmount);
                Command.Parameters.AddWithValue("@memoNo", stock.MemoNo);
                Command.Parameters.AddWithValue("@type", stock.Type);
                Command.Parameters.AddWithValue("@itemSl", stock.ItemSL);
                Connection.Open();
                int rowAffect = Command.ExecuteNonQuery();
                Connection.Close();
                if (rowAffect > 0)
                {
                    return "Update Successfull!";
                }
                else
                {
                    return "Update Failed";
                }
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }




        public string DeletePurchaseItem(Stock purchaseItem)
        {
            string query = "DELETE FROM Stocks WHERE MemoNo =" + purchaseItem.MemoNo + " AND Type='"+ purchaseItem.Type + "' AND ItemSL = "+ purchaseItem.ItemSL + "";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            if (rowAffect > 0)
            {
                return "Delete Successfull!";
            }
            else
            {
                return "Delete Failed";
            }
        }




    }
}