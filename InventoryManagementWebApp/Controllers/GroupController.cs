﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using InventoryManagementWebApp.Models;

namespace InventoryManagementWebApp.Controllers
{
    public class GroupController : Controller
    {
        public SqlConnection Connection { get; set; }
        public SqlCommand Command { get; set; }
        public SqlDataReader Reader { get; set; }

        public GroupController()
        {
            string connectionString = WebConfigurationManager.ConnectionStrings["InventoryContextDB"].ConnectionString;
            Connection = new SqlConnection(connectionString);
        }
        // GET: Group
        public ActionResult SaveGroup()
        {
            return View();
        }


        public JsonResult SaveGroupInfo(Group group)
        {
            int rowAffect = Save(group);
            string msg = String.Empty;
            if (rowAffect > 0)
            {
                msg = "Save Successful";
            }
            else
            {
                msg = "Save Failed";
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public int Save(Group group)
        {
            string query = "INSERT INTO Groups VALUES(@group,@party,@address,@telephone,@mobile,@email,@wAddress,@cpName,@cMobile,@remark)";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@group", group.GroupNm);
            Command.Parameters.AddWithValue("@party", group.PartyNm);
            Command.Parameters.AddWithValue("@address", group.Address);
            Command.Parameters.AddWithValue("@telephone", group.TelephoneNo);
            Command.Parameters.AddWithValue("@mobile", group.MobileNo);
            Command.Parameters.AddWithValue("@email", group.Email);
            Command.Parameters.AddWithValue("@wAddress", group.WebAddress);
            Command.Parameters.AddWithValue("@cpName", group.ContactPersonNm);
            Command.Parameters.AddWithValue("@cMobile", group.ContactMobileNo);
            Command.Parameters.AddWithValue("@remark", group.Remark);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }
    }
}