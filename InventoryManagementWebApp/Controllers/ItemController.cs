﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using InventoryManagementWebApp.Models;

namespace InventoryManagementWebApp.Controllers
{
    public class ItemController : Controller
    {
        public SqlConnection Connection { get; set; }
        public SqlCommand Command { get; set; }
        public SqlDataReader Reader { get; set; }

        public ItemController()
        {
            string connectionString = WebConfigurationManager.ConnectionStrings["InventoryContextDB"].ConnectionString;
            Connection = new SqlConnection(connectionString);
        }
        
        // GET: Item
        public ActionResult SaveItem()
        {
            return View();
        }

        public JsonResult InsertItem(Item item)
        {
            string msg = String.Empty;
            if (IsItemExists(item.ItemName))
            {
                msg = "Item Name Exist!";
            }
            else
            {
                int rowAffect = Save(item);
                if (rowAffect > 0)
                {
                    msg = "Save Successfull!";
                }
                else
                {
                    msg = "Saved Failed";
                }
            }

            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllItem(Category category)
        {
            
            return Json(GetAllCategorieses(category), JsonRequestBehavior.AllowGet);
        }


        public JsonResult DeleteItem(Item items)
        {
            Int64 id = items.ItemId;
            string result = DeleteItemById(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult UpdateItem(Item items)
        {
            string result = UpdateItemById(items);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public int Save(Item item)
        {
            string query = "INSERT INTO Items VALUES(@ItemName,@BrandName,@Unit,@BuyRate,@SaleRate,@Qty,@Discount,@PCQty,@CategoryId)";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@ItemName", item.ItemName);
            Command.Parameters.AddWithValue("@BrandName", item.BrandName);
            Command.Parameters.AddWithValue("@Unit", item.Unit);
            Command.Parameters.AddWithValue("@BuyRate", item.BuyRate);
            Command.Parameters.AddWithValue("@SaleRate", item.SaleRate);
            Command.Parameters.AddWithValue("@Qty", item.Qty);
            Command.Parameters.AddWithValue("@Discount", item.Discount);
            Command.Parameters.AddWithValue("@PCQty", item.PCQty);
            Command.Parameters.AddWithValue("@CategoryId", item.CategoryId);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffect;
        }
        public bool IsItemExists(string itemName)
        {
            string query = "SELECT * FROM Items WHERE ItemName ='" + itemName + "'";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            bool isExists = Reader.HasRows;
            Connection.Close();
            return isExists;
        }


        public List<Item> GetAllCategorieses(Category category)
        {
            //string query = "SELECT * FROM Items ORDER BY ItemId ASC";
            //string query = "SELECT i.ItemName AS Name, i.Unit AS ItemUnit FROM Items AS i INNER JOIN Categories AS c ON i.CategoryId=c.CategoryId WHERE c.CategoryId=35";
            string query = "SELECT * FROM Items AS i INNER JOIN Categories AS c ON i.CategoryId=c.CategoryId WHERE c.CategoryName='"+category.CategoryName+"'";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            List<Item> itemList = new List<Item>();
            while (Reader.Read())
            {
                Item item = new Item();
                item.ItemId = Convert.ToInt32(Reader["ItemId"]);
                item.ItemName = Reader["ItemName"].ToString();
                item.BrandName = Reader["BrandName"].ToString();
                item.Unit = Convert.ToInt32(Reader["Unit"]);
                item.BuyRate = Convert.ToInt32(Reader["BuyRate"]);
                item.SaleRate = Convert.ToInt32(Reader["SaleRate"]);
                item.Qty = Convert.ToInt32(Reader["Qty"]);
                item.Discount = Convert.ToInt32(Reader["Discount"]);
                item.PCQty = Convert.ToInt32(Reader["PCQty"]);
                itemList.Add(item);
            }

            Connection.Close();
            return itemList;

        }

        public string DeleteItemById(Int64 itemId)
        {
            string query = "DELETE FROM Items WHERE ItemId =" + itemId +"";
            Command = new SqlCommand(query, Connection);
            Connection.Open();
            int rowAffect = Command.ExecuteNonQuery();
            Connection.Close();
            if (rowAffect>0)
            {
                return "Delete Successfull!";
            }
            else
            {
                return "Delete Failed";
            }
        }

        public string UpdateItemById(Item items)
        {

            //string query = "Update Items SET ItemName='"+items.ItemName+"',ItemBrand ='"+items.BrandName+"', Unit="+items.Unit+",BuyRate="+items.BuyRate+", SaleRate="+items.SaleRate+", Qty="+items.Qty+", Discount="+items.Discount+", PCQty="+items.PCQty+"   WHERE ItemId =" + items.ItemId + "";
            try
            {
                string query =
                    "UPDATE Items SET ItemName=@IName, BrandName=@IBrand, Unit=@IUnit, BuyRate=@IBuyRate, SaleRate=@ISaleRate, Qty=@IQty, Discount=@IDiscount, PCQty=@IPCQty   WHERE ItemId =@Iid";
                Command = new SqlCommand(query, Connection);
                Command.Parameters.AddWithValue("@IName", items.ItemName);
                Command.Parameters.AddWithValue("@IBrand", items.BrandName);
                Command.Parameters.AddWithValue("@IUnit", items.Unit);
                Command.Parameters.AddWithValue("@IBuyRate", items.BuyRate);
                Command.Parameters.AddWithValue("@ISaleRate", items.SaleRate);
                Command.Parameters.AddWithValue("@IQty", items.Qty);
                Command.Parameters.AddWithValue("@IDiscount", items.Discount);
                Command.Parameters.AddWithValue("@IPCQty", items.PCQty);
                Command.Parameters.AddWithValue("@Iid", items.ItemId);
                Connection.Open();
                int rowAffect = Command.ExecuteNonQuery();
                Connection.Close();
                if (rowAffect > 0)
                {
                    return "Update Successfull!";
                }
                else
                {
                    return "Update Failed";
                }
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

    }
}